<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Quotation;
use App\danhmuc;

class sanpham extends Model
{
    //
    protected $table = "sanpham";
    public function danhmuc()
    {
    	return $this->belongsTo('App\danhmuc','danhmucid','id');
    }
    public static function getAllSanPham()
    {
        return sanpham::paginate(10);
    }
        public static function getSanPhamById($id)
    {
        return sanpham::find($id);
    }
    public static function get4SanPhamMoiNhat()
    {
        return sanpham::orderBy('created_at','desc')->take(4)->get();
    }
    public static function getSanPhamByTheLoai($id)
    {
        return sanpham::where('danhmucid',$id)->paginate(10);
    }
    public static function editSanPham($request,$id)
    {   
        $sanpham = sanpham::where('id',$id);
        $sanpham->update(['ten'=>$request->txtTenSanPham],['tomtat'=>$request->txtTomTat],['noidung'=>$request->txtNoiDung],['gia'=>$request->txtGiaSanPham],['hinhanh'=>$request->txtHinhAnh],['theloaiid'=>$request->txtDanhMuc]);
        if($request->hasFile('txtHinhAnh'))
        {
            $file = $request->file('txtHinhAnh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi == 'jpg' || $duoi == 'JPG' || $duoi == 'png' || $duoi == 'PNG' || $duoi == 'jpeg' || $duoi == 'JPEG')
            {
                $name = $file->getClientOriginalName();
                $Hinh = str_random(4)."_".$name;
                while(file_exists("upload/sanpham/".$Hinh))
                {
                    $Hinh = str_random(4)."_".$name;
                }
                $file->move("upload/sanpham/",$Hinh);
                $sanpham->update(['hinhanh'=>$Hinh]);
            }
            else
            {
                return redirect('admin/sanpham/add')->with('messenger','Bạn chỉ được chọn file ảnh');
            }   
        }
        return $sanpham;
    }
    public static function addSanPham($request)
    {
        $sanpham = new sanpham;
        $sanpham->ten = $request->txtTenSanPham;
        $sanpham->tomtat = $request->txtTomTat;
        $sanpham->noidung = $request->txtNoiDung;
        $sanpham->gia = $request->txtGiaSanPham;
        $sanpham->danhmucid = $request->txtDanhMuc;
        if($request->hasFile('txtHinhAnh'))
        {
            $file = $request->file('txtHinhAnh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi == 'jpg' || $duoi == 'JPG' || $duoi == 'png' || $duoi == 'PNG' || $duoi == 'jpeg' || $duoi == 'JPEG')
            {
                $name = $file->getClientOriginalName();
                $Hinh = str_random(4)."_".$name;
                while(file_exists("upload/sanpham/".$Hinh))
                {
                    $Hinh = str_random(4)."_".$name;
                }
                $file->move("upload/sanpham/",$Hinh);
                $sanpham->hinhanh = $Hinh;
            }
            else
            {
                return redirect('admin/sanpham/add')->with('messenger','Bạn chỉ được chọn file ảnh');
            }
            
        }
        else
        {
            $sanpham->hinhanh = "";
        }
        $sanpham->save();
        return $sanpham;
    }
    public static function deleteSanPham($id)
    {
        $sanpham = sanpham::where('id',$id);
        $sanpham->delete();
        return $sanpham;
    }
}
