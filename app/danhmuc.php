<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;


class danhmuc extends Model
{
    //
    protected $table = "danhmuc";
    public $timestamps = false;
    
    public function cauhoi()
    {
    	return $this->hasMany('App\cauhoi','danhmucid','id');
    }
    public function sanpham()
    {
        return $this->hasMany('App\cauhoi','danhmucid','id');
    }
    public static function getAllDanhMuc()
    {
    	return danhmuc::all();
    }
    public static function getAllDanhMucSanPham()
    {
        return danhmuc::where('menu',1)->get();
    }
    public static function getAllDanhMucHoiDap()
    {
        return danhmuc::where('menu',0)->get();
    }
    //Edit
    public static function getDanhMucById($id)
    {
        return danhmuc::find($id);
    }
    public static function editDanhMuc($request,$id)
    {
        $danhmuc = danhmuc::where('id',$id);
        $danhmuc->update(['ten'=>$request->txtTenDanhMuc],['menu'=>$request->txtMenu]);
        return $danhmuc;
    }
    public static function addDanhMuc($request)
    {
        $danhmuc = new danhmuc;
        $danhmuc->ten = $request->txtTenDanhMuc;
        $danhmuc->menu = $request->txtMenu;
        $danhmuc->save();
        return $danhmuc;
    }
    public static function deleteDanhMuc($id)
    {
        $danhmuc = danhmuc::where('id',$id);
        $danhmuc->delete();;
        return $danhmuc;
    }

}
