<?php

namespace App\Http\Controllers;

use App\cauhoi;
use App\danhmuc;
use App\User;
use App\traloi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class HoiDapController extends Controller
{
    //
    public function __construct()
    {
    	$danhmuc = danhmuc::getAllDanhMucHoiDap();
        $cauhoimoi = cauhoi::get5CauHoiMoiNhat();
    	view()->share('danhmuc',$danhmuc);
        view()->share('cauhoimoi',$cauhoimoi);
    }
    public function getAllCauHoi()
    {
        
    	$cauhoi = cauhoi::getAllCauHoi();
    	$cauhoinotraloi = cauhoi::getAllCauNoTraLoi();
    	return view('pages.hoidap.hoidap',['cauhoi'=>$cauhoi],['cauhoinotraloi'=>$cauhoinotraloi]);
    }
    public function getCauHoiByDanhMuc($id)
    {
    	$cauhoibydanhmuc = cauhoi::getCauHoiByDanhMuc($id);
        $cauhoinotraloibydanhmuc = cauhoi::getCauHoiNoTraLoiByDanhMuc($id);
    	return view('pages.hoidap.danhmuc',['cauhoibydanhmuc' => $cauhoibydanhmuc],['cauhoinotraloibydanhmuc'=>$cauhoinotraloibydanhmuc]);
    }
    public function getAddCauHoi()
    {
        return view('pages.hoidap.datcauhoi');
    }
    public function postAddCauHoi(Request $request)
    {
        $cauhoi = cauhoi::postAddCauHoi($request);
        return redirect('hoi-dap/dat-cau-hoi')->with('thongbao','Gửi câu hỏi thành công');
    }
    public function getCauHoiById($id)
    {
        $cauhoi = cauhoi::getCauHoiById($id);
        $traloi = traloi::getTraLoiByIdCauHoi($id);
        return view('pages.hoidap.cauhoidetail',['cauhoi'=>$cauhoi],['traloi'=>$traloi]);
    }
    public function postTraLoiByIdCauHoi(Request $request,$id)
    {
        if(Auth::check())
        {
            $traloi = traloi::postTraLoiByIdCauHoi($request,$id);
            return redirect('hoi-dap/cau-hoi/'.$id)->with('thongbao','Viết bình luận thành công');
        }
        else 
            return redirect('hoi-dap/cau-hoi/'.$id)->with('thongbaoloi','Bạn cần phải đăng nhập để viết bình luận');
    }
}
