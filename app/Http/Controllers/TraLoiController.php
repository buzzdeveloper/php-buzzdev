<?php

namespace App\Http\Controllers;

use App\cauhoi;
use App\traloi;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class TraLoiController extends Controller
{
    //
    public function getAddTraLoi($idcauhoi)
    {
    	$cauhoi = traloi::getAddTraLoi($idcauhoi);
    	return view('admin.test',['cauhoi'=>$cauhoi]);
    }
    public function postAddTraLoi(Request $request,$idcauhoi)
    {
    	$this->validate($request,[
                'txtNoiDung' => 'required|min:20'
            ],[
                'txtNoiDung.required' => 'Bạn chưa nhập nội dung câu trả lời',
                'txtNoiDung.min' => 'Nội dung câu trả lời ít nhất là 20 ký tự'
            ]);
    	$traloi = traloi::postAddTraLoi($request,$idcauhoi);
    	return redirect('admin/test/'.$idcauhoi)->with('thongbao','trả lời thành công');
    }
}
