<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;

class UserController extends Controller
{
    //
    public function getList()
    {
    	$users = User::getAllDanhSachUser();
        return view('admin.users.list',['users'=>$users]);
    }
    public function getEdit($id)
    {
    	$users = User::getUserById($id);
        return view('admin.users.edit',['users'=>$users]);
    }
    public function postEdit(Request $request,$id)
    {
    	$this->validate($request,[
    			'txtTaiKhoan' => 'required|min:3|max:50',
    			'txtEmail' => 'unique:users,email',
    			'txtMatKhau' => 'required|min:6|max:32',
    			'txtNhapLaiMatKhau' => 'required|same:txtMatKhau',
    			'txtLevel' => 'required'

    		],[
    			'txtTaiKhoan.required' => 'Bạn chưa nhập tên tài khoản',
    			'txtTaiKhoan.unique' => 'Tên tài khoản tồn tại',
    			'txtTaiKhoan.min' => 'Tên tài khoản ít nhất là 3 ký tự',
    			'txtTaiKhoan.max' => 'Tên tài khoản không được quá 50 ký tự',
    			'txtEmail.unique' => 'Email đã tồn tại',
    			'txtMatKhau.required' => 'Bạn chưa nhập mật khẩu',
    			'txtMatKhau.min' => 'Mật khẩu ít nhất 6 ký tự',
    			'txtMatKhau.max' => 'Mật khẩu không được quá 32 ký tự',
    			'txtNhapLaiMatKhau.required' => 'Bạn chưa nhập lại mật khẩu',
    			'txtNhapLaiMatKhau.same' => 'Nhập lại mật khẩu không khớp',
    			'txtLevel' => 'Bạn chưa nhập level'
    		]);
        $users = User::editUser($request,$id);
    	return redirect('admin/users/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
        return view('admin.users.add');
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTaiKhoan' => 'required|min:3|max:50',
    			'txtEmail' => 'unique:users,email',
    			'txtMatKhau' => 'required|min:6|max:32',
    			'txtNhapLaiMatKhau' => 'required|same:txtMatKhau',
    			'txtLevel' => 'required'

    		],[
    			'txtTaiKhoan.required' => 'Bạn chưa nhập tên tài khoản',
    			'txtTaiKhoan.unique' => 'Tên tài khoản tồn tại',
    			'txtTaiKhoan.min' => 'Tên tài khoản ít nhất là 3 ký tự',
    			'txtTaiKhoan.max' => 'Tên tài khoản không được quá 50 ký tự',
    			'txtEmail.unique' => 'Email đã tồn tại',
    			'txtMatKhau.required' => 'Bạn chưa nhập mật khẩu',
    			'txtMatKhau.min' => 'Mật khẩu ít nhất 6 ký tự',
    			'txtMatKhau.max' => 'Mật khẩu không được quá 32 ký tự',
    			'txtNhapLaiMatKhau.required' => 'Bạn chưa nhập lại mật khẩu',
    			'txtNhapLaiMatKhau.same' => 'Nhập lại mật khẩu không khớp',
    			'txtLevel' => 'Bạn chưa nhập level'
    		]);
    	$users = User::addUser($request);
    	return redirect('admin/users/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	$users = User::deleteUser($id);
    	return redirect('admin/users/list')->with('thongbao','Đã xóa thể loại '.$users->name);
    }
}
