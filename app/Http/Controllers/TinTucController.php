<?php

namespace App\Http\Controllers;

use App\tintuc;
use App\theloai;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class TinTucController extends Controller
{

    public function __construct()
    {
        $theloaitin = theloai::getAllTheLoai();
        view()->share(['theloaitin'=>$theloaitin]);
    }
    public function getAllTinTuc($id)
    {
        $tintuc = tintuc::getAllTinTuc($id);
        return view('pages.tintuc.tintuc',['tintuc'=>$tintuc]);
    }
    //
    public function getDetail($id)
    {
        $tintuc = tintuc::getTinById($id);
        $tinlienquan = tintuc::get5TinByTheLoai($id);
        return view('pages.tintuc.detail',['tintuc'=>$tintuc],['tinlienquan'=>$tinlienquan]);
    }

    //List
    //ADMIN___________________________________________________________________________
    public function getList()
    {
    	$tintuc = tintuc::getAll();
        return view('admin.tintuc.list',['tintuc'=>$tintuc]);
    }
    //Edit
    public function getEdit($id)
    {
    	$tintuc = tintuc::getTinById($id);
    	$theloai = theloai::getAllTheLoai();
        return view('admin.tintuc.edit',['tintuc'=>$tintuc,'theloai'=>$theloai]);
    }
    public function postEdit(Request $request,$id)
    {    	
        $this->validate($request,[
    			'txtTieuDe' => 'required|min:3|max:100|unique:tintuc,tieude,'.$id,
    			'txtTomTat' => 'required|min:50',
    			'txtNoiDung' => 'required|min:50',
    		],[
    			'txtTieuDe.required' => 'Bạn chưa nhập tên tiêu đề',
    			'txtTieuDe.unique' => 'Tên tiêu đề đã tồn tại',
    			'txtTieuDe.min' => 'Tên tiêu đề ít nhất là 3 ký tự',
    			'txtTieuDe.max' => 'Tên tiêu đề không được vượt quá 100 ký tự',
    			'txtTomTat.required' => 'Bạn chưa nhập tóm tắt',
    			'txtTomTat.min' => 'Tóm tắt ít nhất 50 ký tự',
    			'txtNoiDung.required' => 'Bạn chưa nhập nội dung',
    			'txtNoiDung.min' => 'Nội dung ít nhất là 50 ký tự',
    		]);
        tintuc::editTinTuc($request,$id);
    	return redirect('admin/tintuc/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
    	$theloai = theloai::getAllTheLoai();
        return view('admin.tintuc.add',['theloai' => $theloai]);
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTieuDe' => 'required|unique:tintuc,tieude|min:3|max:150',
    			'txtTomTat' => 'required|min:50',
    			'txtNoiDung' => 'required|min:50',
    		],[
    			'txtTieuDe.required' => 'Bạn chưa nhập tên tiêu đề',
    			'txtTieuDe.unique' => 'Tên tiêu đề đã tồn tại',
    			'txtTieuDe.min' => 'Tên tiêu đề ít nhất là 3 ký tự',
    			'txtTieuDe.max' => 'Tên tiêu đề không được vượt quá 150 ký tự',
    			'txtTomTat.required' => 'Bạn chưa nhập tóm tắt',
    			'txtTomTat.min' => 'Tóm tắt ít nhất 50 ký tự',
    			'txtNoiDung.required' => 'Bạn chưa nhập nội dung',
    			'txtNoiDung.min' => 'Nội dung ít nhất là 50 ký tự',
    		]);
    	tintuc::addTinTuc($request);
    	return redirect('admin/tintuc/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	tintuc::deleteTinTuc($id);
    	return redirect('admin/tintuc/list')->with('thongbao','Đã xóa tin tức');
    }
}
