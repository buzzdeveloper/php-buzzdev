<?php

namespace App\Http\Controllers;

use App\theloai;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class TheLoaiController extends Controller
{
	//List
    public function getList()
    {
    	$theloai = theloai::getAllTheLoai();
        return view('admin.theloai.list',['theloai'=>$theloai]);
    }
    //Edit
    public function getEdit($id)
    {
    	$theloai = theloai::getTheLoaiById($id);

        return view('admin.theloai.edit',['theloai'=>$theloai]);
    }
    public function postEdit(Request $request,$id)
    {
    	$this->validate($request,[
    			'txtTenTheLoai' => 'required|min:3|max:100|unique:theloai,ten,'.$id,
    		],[
    			'txtTenTheLoai.required' => 'Bạn chưa nhập tên thể loại',
    			'txtTenTheLoai.unique' => 'Tên thể loại đã tồn tại',
    			'txtTenTheLoai.min' => 'Tên thể loại ít nhất là 3 ký tự',
    			'txtTenTheLoai.max' => 'Tên thể loại không được vượt quá 50 ký tự',
    		]);
        theloai::editTheLoai($request,$id);
    	return redirect('admin/theloai/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
        return view('admin.theloai.add');
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTenTheLoai' => 'required|unique:theloai,ten|min:3|max:50'
    		],[
    			'txtTenTheLoai.required' => 'Bạn chưa nhập tên thể loại',
    			'txtTenTheLoai.unique' => 'Tên thể loại đã tồn tại',
    			'txtTenTheLoai.min' => 'Tên thể loại ít nhất là 3 ký tự',
    			'txtTenTheLoai.max' => 'Tên thể loại không được vượt quá 50 ký tự',
    		]);
    	theloai::addTheLoai($request);
    	return redirect('admin/theloai/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	$theloai = theloai::deleteTheLoai($id);
    	return redirect('admin/theloai/list')->with('thongbao','Đã xóa thể loại');
    }
}
