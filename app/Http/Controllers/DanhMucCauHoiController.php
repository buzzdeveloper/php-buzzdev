<?php

namespace App\Http\Controllers;

use App\danhmuccauhoi;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class DanhMucCauHoiController extends Controller
{
    //List
    public function getList()
    {
    	$danhmuccauhoi = danhmuccauhoi::all();
        return view('admin.danhmuccauhoi.list',['danhmuccauhoi'=>$danhmuccauhoi]);
    }
    //Edit
    public function getEdit($id)
    {
    	$danhmuccauhoi = danhmuccauhoi::find($id);

        return view('admin.danhmuccauhoi.edit',['danhmuccauhoi'=>$danhmuccauhoi]);
    }
    public function postEdit(Request $request,$id)
    {
    	$this->validate($request,[
    			'txtTenDanhMuc' => 'required|unique:danhmuccauhoi,ten|min:3|max:50'
    		],[
    			'txtTenDanhMuc.required' => 'Bạn chưa nhập tên danh mục',
    			'txtTenDanhMuc.unique' => 'Tên danh mục đã tồn tại',
    			'txtTenDanhMuc.min' => 'Tên danh mục ít nhất là 3 ký tự',
    			'txtTenDanhMuc.max' => 'Tên danh mục không được vượt quá 50 ký tự',
    		]);
        $danhmuccauhoi = danhmuccauhoi::where('id',$id);
        if(!empty($danhmuccauhoi))
        {
    	   $danhmuccauhoi->update(['ten'=>$request->txtTenDanhMuc]);
        }
    	return redirect('admin/danhmuccauhoi/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
        return view('admin.danhmuccauhoi.add');
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTenDanhMuc' => 'required|unique:danhmuccauhoi,ten|min:3|max:50'
    		],[
    			'txtTenDanhMuc.required' => 'Bạn chưa nhập tên danh mục',
    			'txtTenDanhMuc.unique' => 'Tên danh mục đã tồn tại',
    			'txtTenDanhMuc.min' => 'Tên danh mục ít nhất là 3 ký tự',
    			'txtTenDanhMuc.max' => 'Tên danh mục không được vượt quá 50 ký tự',
    		]);
    	$danhmuccauhoi = new danhmuccauhoi;
    	$danhmuccauhoi->ten = $request->txtTenDanhMuc;
    	$danhmuccauhoi->save();
    	return redirect('admin/danhmuccauhoi/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	$danhmuccauhoi = danhmuccauhoi::where('id',$id);
        $danhmuccauhoi->delete();;
    	return redirect('admin/danhmuccauhoi/list')->with('thongbao','Đã xóa danh mục');
    }

    public function pageHoiDap()
    {
        return View('pages.hoidap.hoidap');
    }
    public function traLoiHoiDap()
    {
        return View('pages.hoidap.tencauhoi');
    }
}
