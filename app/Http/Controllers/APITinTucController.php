<?php

namespace App\Http\Controllers;

use App\tintuc;
use App\theloai;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class APITinTucController extends Controller
{
    public function apiGetAllTheLoai()
    {
        $theloaitin = theloai::getAllTheLoai();
        if(!empty($theloaitin))
	        return response()->json($arrayName = array(
	    		'success' => true,
	    		'data' => $theloaitin,
	    		 ));
	    else
	    	return response()->json($arrayName = array(
	    		'success' => false,
	    		 ));
    }
    public function apiGetAllTinTuc($id,$page)
    {
        $tintuc = tintuc::apiGetAllTinTuc($id,$page);
        $total = tintuc::countTotal($id);
        if(!empty($tintuc))
	        return response()->json($arrayName = array(
	    		'success' => true,
	    		'data' => $tintuc,
	    		'Total' => $total
	    		 ));
	    else
	    	return response()->json($arrayName = array(
	    		'success' => false,
	    		 ));
    }
    //
    public function getDetail($id)
    {
        $tintuc = tintuc::getTinTucById($id);
        if(!empty($tintuc))
	        return response()->json($arrayName = array(
	    		'success' => true,
	    		'data' => $tintuc
	    		 ));
	    else
	    	return response()->json($arrayName = array(
	    		'success' => false,
	    		 ));
    }
}
