<?php

namespace App\Http\Controllers;

use App\theloai;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class AjaxController extends Controller
{
    //
    public function getTheLoai($idMenu)
    {
    	$theloai = theloai::where('menu',$idMenu)->get();
    	foreach ($theloai as $tl) {
    		echo "<option value='".$tl->id."'>".$tl->ten."</option>";
    	}
    }
}
