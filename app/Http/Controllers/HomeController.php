<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\tintuc;
use App\User;
use App\sanpham;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class HomeController extends Controller
{
    //
    public function index()
    {
        $camnang = tintuc::get6CamNangMoiNhat();
        $tintuc = tintuc::get6TinMoiNhat();
        $sanpham = sanpham::get4SanPhamMoiNhat();
        $thongbao = tintuc::get5ThongBaoMoiNhat();
        return view('pages.index',['tintuc'=>$tintuc,'sanpham'=>$sanpham,'camnang'=>$camnang,'thongbao'=>$thongbao]);
    }



    //Đăng nhập user
    public function getRegister()
    {
    	return view('pages.users.register');
    }
    public function postRegister(Request $request)
    {
    	$this->validate($request,[
    			'txtTaiKhoan' => 'required|min:3|max:50',
    			'txtEmail' => 'unique:users,email',
    			'txtPassWord' => 'required|min:6|max:32',
    			'passwordAgain' => 'required|same:txtPassWord',

    		],[
    			'txtTaiKhoan.required' => 'Bạn chưa nhập tên tài khoản',
    			'txtTaiKhoan.unique' => 'Tên tài khoản tồn tại',
    			'txtTaiKhoan.min' => 'Tên tài khoản ít nhất là 3 ký tự',
    			'txtTaiKhoan.max' => 'Tên tài khoản không được quá 50 ký tự',
    			'txtEmail.unique' => 'Email đã tồn tại',
    			'txtPassWord.required' => 'Bạn chưa nhập mật khẩu',
    			'txtPassWord.min' => 'Mật khẩu ít nhất 6 ký tự',
    			'txtPassWord.max' => 'Mật khẩu không được quá 32 ký tự',
    			'passwordAgain.required' => 'Bạn chưa nhập lại mật khẩu',
    			'passwordAgain.same' => 'Nhập lại mật khẩu không khớp'
    		]);
    	$user = User::postRegister($request);

    	return redirect('trang-chu');
    }
    public function postLogin(Request $request)
    {
    	$user = User::postLogin($request);

    	if($user == 1)
    		return redirect('trang-chu');
    	else
    		return redirect('trang-chu')->with('thongbao','Đăng nhập thất bại');
    }
    public function getLogout()
    {
    	Auth::logout();

    	return redirect('trang-chu');
    }
}
