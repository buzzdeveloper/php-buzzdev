<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;

class AdminLoginController extends Controller
{
    public function getLogin()
    {
    	return view('admin.login');
    }
    public function postLogin(Request $request)
    {
    	$this->validate($request,[
    			'txtEmail' => 'required',
    			'txtPassWord' => 'required|min:5|max:32'
    		],[
    			'txtEmail.required' => 'Bạn chưa nhập Email',
    			'txtPassWord.required' => 'Bạn chưa nhập PassWord',
    			'txtPassWord.min' => 'Mật khẩu không được ít hơn 5 ký tự',
    			'txtPassWord.max' => 'Mật khẩu không được dài hơn 32 ký tự'
    		]);
    	if(Auth::attempt(['email' => $request->txtEmail,'password' => $request->txtPassWord]))
    	{
    		return redirect('admin/theloai/list');
    	}
    	else
    	{
            $user = Auth::user();
            if(empty($user))
            {
                
                return redirect('admin/login')->with('thongbao','ko có user');
            }
            else
            {
               
                return redirect('admin/login')->with('thongbao','Đăng nhập thất bại');
            }
    	}

    }
}
