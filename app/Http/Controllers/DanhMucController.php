<?php

namespace App\Http\Controllers;

use App\danhmuc;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class DanhMucController extends Controller
{
    //List
    public function getList()
    {
    	$danhmuc = danhmuc::getAllDanhMuc();
        return view('admin.danhmuc.list',['danhmuc'=>$danhmuc]);
    }
    //Edit
    public function getEdit($id)
    {
    	$danhmuc = danhmuc::getDanhMucById($id);

        return view('admin.danhmuc.edit',['danhmuc'=>$danhmuc]);
    }
    public function postEdit(Request $request,$id)
    {
    	$this->validate($request,[
    			'txtTenDanhMuc' => 'required|unique:danhmuc,ten|min:3|max:50'
    		],[
    			'txtTenDanhMuc.required' => 'Bạn chưa nhập tên danh mục',
    			'txtTenDanhMuc.unique' => 'Tên danh mục đã tồn tại',
    			'txtTenDanhMuc.min' => 'Tên danh mục ít nhất là 3 ký tự',
    			'txtTenDanhMuc.max' => 'Tên danh mục không được vượt quá 50 ký tự',
    		]);
        danhmuc::editDanhMuc($request,$id);
    	return redirect('admin/danhmuc/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
        return view('admin.danhmuc.add');
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTenDanhMuc' => 'required|unique:danhmuc,ten|min:3|max:50'
    		],[
    			'txtTenDanhMuc.required' => 'Bạn chưa nhập tên danh mục',
    			'txtTenDanhMuc.unique' => 'Tên danh mục đã tồn tại',
    			'txtTenDanhMuc.min' => 'Tên danh mục ít nhất là 3 ký tự',
    			'txtTenDanhMuc.max' => 'Tên danh mục không được vượt quá 50 ký tự',
    		]);
    	danhmuc::addDanhMuc($request);
    	return redirect('admin/danhmuc/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	$danhmuc = danhmuc::deleteDanhMuc($id);
    	return redirect('admin/danhmuc/list')->with('thongbao','Đã xóa danh mục'.$danhmuc->ten);
    }

}
