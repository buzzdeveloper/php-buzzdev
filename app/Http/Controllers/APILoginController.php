<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APILoginController extends Controller
{
    public function store(Request $request)
    {
    	$users = User::postRegister($request);
    	if(!empty($users))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $users
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $users
                 ));
    }
    public function update(Request $request,$id)
    {
        $users = User::editUser($request,$id);
    	if(!empty($users))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $users
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $users
                 ));
    }
    public function postLogin(Request $request)
    {
    	$users = User::getLogin($request);

    	if($users == 1)
    		return response()->json($arrayName = array(
    		'success' => true,
    		'token' => Auth::user(),
    		 ));
    	else
    		return response()->json($arrayName = array(
    		'success' => false,
    		 ));
    }
}
