<?php

namespace App\Http\Controllers;

use App\cauhoi;
use App\danhmuc;
use App\User;
use App\traloi;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

class APIHoiDapController extends Controller
{
    public function __construct()
    {
    	$danhmuc = danhmuc::getAllDanhMucHoiDap();
        if(!empty($danhmuc))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $danhmuc
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $danhmuc
                 ));
    }
    public function apiGetAllCauHoi($page)
    {  
    	$cauhoi = cauhoi::apiGetAllCauHoi($page);
        $total = cauhoi::countCauHoi();
    	if(!empty($cauhoi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoi,
                'Total' => $total
                 ));
    }
    public function apiGetAllCauNoTraLoi($page)
    {
        $cauhoinotraloi = cauhoi::apiGetAllCauNoTraLoi($page);
        $total = cauhoi::countCauHoiNoTraLoi();
        if(!empty($cauhoinotraloi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoinotraloi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoinotraloi,
                'Total' => $total
                 ));
    }
    public function apiGetCauHoiByDanhMuc($id,$page)
    {
    	$cauhoibydanhmuc = cauhoi::apiGetCauHoiByDanhMuc($id,$page);
        $total = cauhoi::countCauHoiByDanhMuc($id);
    	if(!empty($cauhoibydanhmuc))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoibydanhmuc,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoibydanhmuc,
                'Total' => $total
                 ));
    }
    public function apiGetCauHoiNoTraLoiByDanhMuc($id,$page)
    {
        $cauhoinotraloibydanhmuc = cauhoi::apiGetCauHoiNoTraLoiByDanhMuc($id,$page);
        $total = cauhoi::countCauHoiNoTraLoiByDanhMuc($id);
        if(!empty($cauhoinotraloibydanhmuc))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoinotraloibydanhmuc,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoinotraloibydanhmuc,
                'Total' => $total
                 ));
    }
    public function postAddCauHoi(Request $request)
    {
        $cauhoi = cauhoi::postAddCauHoi($request);
        if(!empty($cauhoi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoi,
                'Total' => $total
                 ));
    }
    public function getCauHoiById($id)
    {
        $cauhoi = cauhoi::getCauHoiById($id);
        if(!empty($cauhoi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $cauhoi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $cauhoi,
                'Total' => $total
                 ));
    }
    public function getTraLoiByIdCauHoi($id)
    {
        $traloi = traloi::getTraLoiByIdCauHoi($id);
        if(!empty($traloi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $traloi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $traloi,
                'Total' => $total
                 ));
    }
    public function postTraLoiByIdCauHoi(Request $request,$id)
    {
        $traloi = traloi::postTraLoiByIdCauHoi($request,$id);
        if(!empty($traloi))
            return response()->json($arrayName = array(
                'success' => true,
                'data' => $traloi,
                'Total' => $total
                 ));
        else
            return response()->json($arrayName = array(
                'success' => false,
                'data' => $traloi,
                'Total' => $total
                 ));
    }
}
