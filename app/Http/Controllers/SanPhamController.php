<?php

namespace App\Http\Controllers;

use App\sanpham;
use App\danhmuc;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class SanPhamController extends Controller
{
    public function __construct()
    {
        $danhmucsanpham = danhmuc::getAllDanhMucSanPham();
        view()->share(['danhmucsanpham'=>$danhmucsanpham]);
    }
    public function getAllSanPham()
    {
        $sanpham = sanpham::getAllSanPham();
        return view('pages.sanpham.sanpham',['sanpham'=>$sanpham]);
    }
    public function getSanPhamByTheLoai($id)
    {
        $sanpham = sanpham::getSanPhamByTheLoai($id);
        return view('pages.sanpham.danhmuc',['sanpham'=>$sanpham]);
    }
    public function getSanPhamById($id)
    {
        $sanpham = sanpham::getSanPhamById($id);
        return view('pages.sanpham.sanphamdetail',['sanpham'=>$sanpham]);
    }




    //ADMIN________________________________________________________________________
    //List
    public function getList()
    {
    	$sanpham = sanpham::getAllSanPham();
    	// echo $sanpham;
        return view('admin.sanpham.list',['sanpham'=>$sanpham]);
    }
    //Edit
    public function getEdit($id)
    {
    	$sanpham = sanpham::getSanPhamById($id);
    	$danhmucsanpham = danhmuc::getAllDanhMucSanPham();
        return view('admin.sanpham.edit',['sanpham'=>$sanpham,'danhmucsanpham'=>$danhmucsanpham]);
    }
    public function postEdit(Request $request,$id)
    {    	
        $this->validate($request,[
    			'txtTenSanPham' => 'required|min:3|max:100|unique:sanpham,ten,'.$id,
    			'txtTomTat' => 'required|min:50',
    			'txtNoiDung' => 'required|min:50',
    			'txtGiaSanPham' => 'required'
    		],[
    			'txtTenSanPham.required' => 'Bạn chưa nhập tên sản phẩm',
    			'txtTenSanPham.unique' => 'Tên sản phẩm đã tồn tại',
    			'txtTenSanPham.min' => 'Tên sản phẩm ít nhất là 3 ký tự',
    			'txtTenSanPham.max' => 'Tên sản phẩm không được vượt quá 150 ký tự',
    			'txtTomTat.required' => 'Bạn chưa nhập tóm tắt',
    			'txtTomTat.min' => 'Tóm tắt ít nhất 50 ký tự',
    			'txtNoiDung.required' => 'Bạn chưa nhập nội dung',
    			'txtNoiDung.min' => 'Nội dung ít nhất là 50 ký tự',
    			'txtGiaSanPham.required' => 'Bạn chưa nhập giá sản phẩm'
    		]);
        sanpham::editSanPham($request,$id);
    	return redirect('admin/sanpham/edit/'.$id)->with('thongbao','Sửa thành công');
    }
    //Add
    public function getAdd()
    {
    	$danhmucsanpham = danhmuc::getAllDanhMucSanPham();
        return view('admin.sanpham.add',['danhmucsanpham' => $danhmucsanpham]);
    }
    public function postAdd(Request $request)
    {
    	$this->validate($request,[
    			'txtTenSanPham' => 'required|unique:sanpham,ten|min:3|max:150',
    			'txtTomTat' => 'required|min:50',
    			'txtNoiDung' => 'required|min:50',
    			'txtGiaSanPham' => 'required'
    		],[
    			'txtTenSanPham.required' => 'Bạn chưa nhập tên sản phẩm',
    			'txtTenSanPham.unique' => 'Tên sản phẩm đã tồn tại',
    			'txtTenSanPham.min' => 'Tên sản phẩm ít nhất là 3 ký tự',
    			'txtTenSanPham.max' => 'Tên sản phẩm không được vượt quá 150 ký tự',
    			'txtTomTat.required' => 'Bạn chưa nhập tóm tắt',
    			'txtTomTat.min' => 'Tóm tắt ít nhất 50 ký tự',
    			'txtNoiDung.required' => 'Bạn chưa nhập nội dung',
    			'txtNoiDung.min' => 'Nội dung ít nhất là 50 ký tự',
    			'txtGiaSanPham.required' => 'Bạn chưa nhập giá sản phẩm'
    		]);
    	sanpham::addSanPham($request);
    	return redirect('admin/sanpham/add')->with('thongbao','Thêm thành công');
    }
    public function getDelete($id)
    {
    	sanpham::deleteSanPham($id);
 
    	return redirect('admin/sanpham/list')->with('thongbao','Đã xóa sản phẩm');
    }
}
