<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function cauhoi()
    {
        return $this->hasMany('App\cauhoi','nguoihoiid','id');
    }
    public function traloi()
    {
        return $this->hasMany('App\traloi','nguoitraloiid','id');
    }
    public static function postRegister($request)
    {
        $user = new User();
        $user->name = $request->txtTaiKhoan;
        $user->email = $request->txtEmail;
        $user->password = bcrypt($request->txtPassWord);
        $user->level = '0';
        $user->save();

        return $user;
    }
    public static function postLogin($request)
    {
        if(Auth::attempt(['email' => $request->txtEmail,'password' => $request->txtPassWord]))
        {
            return true;
        }
        else
        {
            $user = Auth::user();
            if(empty($user))
            {  
                return false;
            }
        }
    }
    //Admin
    public static function getAllDanhSachUser()
    {
        return User::all();
    }
    //Edit
    public static function getUserById($id)
    {
        return User::find($id);
    }
    public static function editUser($request,$id)
    {
        $users = User::find($id);
        $users->name = $request->txtTaiKhoan;
        $users->email = $request->txtEmail;
        $users->password = bcrypt($request->txtMatKhau);
        $users->level = $request->txtLevel;
        $users->save();
        return $users;
    }
    public static function addUser($request)
    {
        $users = new User;
        $users->name = $request->txtTaiKhoan;
        $users->email = $request->txtEmail;
        $users->password = bcrypt($request->txtMatKhau);
        $users->level = $request->txtLevel;
        $users->save();
        return $users;
    }
    public static function deleteUser($id)
    {
        $users = User::find($id);
        $users->delete($id);
        return $users;
    }
}
