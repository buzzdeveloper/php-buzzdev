<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Quotation;
use App\theloai;

class tintuc extends Model
{
    //
    protected $table = "tintuc";
    public function theloai()
    {
    	return $this->belongsTo('App\theloai','theloaiid','id');
    }
    //
    public static function get6TinMoiNhat()
    {
    	return tintuc::where('theloaiid',1)->orderBy('created_at', 'desc')->take(6)->get();
    }
    public static function get6CamNangMoiNhat()
    {
        return tintuc::where('theloaiid',3)->orderBy('created_at', 'desc')->take(6)->get();
    }
    public static function get5ThongBaoMoiNhat()
    {
        return tintuc::where('theloaiid',2)->orderBy('created_at', 'desc')->take(6)->get();
    }
    //
    public static function getAll()
    {
        return tintuc::all();
    }
    public static function getAllTinTuc($id)
    {
        return tintuc::where('theloaiid',$id)->paginate(10);
    }
    //
    public static function getTinById($id)
    {
    	return tintuc::find($id);
    }
    //
    public static function get5TinByTheLoai($id)
    {
        $theloai = tintuc::find($id);
    	return tintuc::where('theloaiid',$theloai->theloaiid)->take(5)->get();
    }
    //
    public static function editTinTuc($request,$id)
    {
        $tintuc = tintuc::where('id',$id);
        $tintuc->update(['tieude'=>$request->txtTieuDe],['tieudekhongdau'=>changeTitle($request->txtTieuDe)],['tomtat'=>$request->txtTomTat],['noidung'=>$request->txtNoiDung],['theloaiid'=>$request->txtTheLoai]);
        if($request->hasFile('txtHinhAnh'))
        {
            $file = $request->file('txtHinhAnh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi == 'jpg' || $duoi == 'JPG' || $duoi == 'png' || $duoi == 'PNG' || $duoi == 'jpeg' || $duoi == 'JPEG')
            {
                $name = $file->getClientOriginalName();
                $Hinh = str_random(4)."_".$name;
                while(file_exists("upload/tintuc/".$Hinh))
                {
                    $Hinh = str_random(4)."_".$name;
                }
                $file->move("upload/tintuc/",$Hinh);
                $tintuc->update(['hinhanh'=>$Hinh]);
            }
            else
            {
                return redirect('admin/tintuc/add')->with('messenger','Bạn chỉ được chọn file ảnh');
            }   
        }

        return $tintuc;
    }
    public static function addTinTuc($request)
    {
        $tintuc = new tintuc;
        $tintuc->tieude = $request->txtTieuDe;
        $tintuc->tieudekhongdau = changeTitle($request->txtTieuDe);
        $tintuc->tomtat = $request->txtTomTat;
        $tintuc->noidung = $request->txtNoiDung;
        $tintuc->theloaiid = $request->txtTheLoai;
        if($request->hasFile('txtHinhAnh'))
        {
            $file = $request->file('txtHinhAnh');
            $duoi = $file->getClientOriginalExtension();
            if($duoi == 'jpg' || $duoi == 'JPG' || $duoi == 'png' || $duoi == 'PNG' || $duoi == 'jpeg' || $duoi == 'JPEG')
            {
                $name = $file->getClientOriginalName();
                $Hinh = str_random(4)."_".$name;
                while(file_exists("upload/tintuc/".$Hinh))
                {
                    $Hinh = str_random(4)."_".$name;
                }
                $file->move("upload/tintuc/",$Hinh);
                $tintuc->hinhanh = $Hinh;
            }
            else
            {
                return redirect('admin/tintuc/add')->with('messenger','Bạn chỉ được chọn file ảnh');
            }
        }
        else
        {
            $tintuc->hinhanh = "";
        }
        $tintuc->save();
        return $tintuc;
    }
    public static function deleteTinTuc($id)
    {
        $tintuc = tintuc::where('id',$id);
        $tintuc->delete();
        return $tintuc;
    }
    //API

    public static function countTotal($id)
    {
        return tintuc::where('theloaiid',$id)->count();
    }
    public static function apiGetAllTinTuc($id,$page)
    {
        if(isset($page))
            $page = 1;
        return tintuc::where('theloaiid',$id)->skip(($page-1)*5)->take(5)->get();
    }
 }
