<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\Quotation;

class theloai extends Model
{
    //
    protected $table = "theloai";
    public $timestamps = false;
    public function tintuc()
    {
    	return $this->hasMany('App\tintuc','theloaiid','id');
    }
    public static function getAllTheLoai()
    {
    	return theloai::all();
    }
    public static function getAllTheLoaiTinTuc()
    {
        return theloai::where('id',1)->get();
    }
    public static function getAllTheLoaiThongBao()
    {
        return theloai::where('id',2)->get();
    }
    public static function getAllTheLoaiCamNang()
    {
        return theloai::where('id',3)->get();
    }
    //Edit
    public static function getTheLoaiById($id)
    {
        return theloai::find($id);
    }
    public static function editTheLoai($request,$id)
    {
        $theloai = theloai::where('id',$id);
        if(!empty($theloai))
        {
           $theloai->update(['ten'=>$request->txtTenTheLoai]);
        }
        return $theloai;
    }
    public static function addTheLoai($request)
    {
        $theloai = new theloai;
        $theloai->ten = $request->txtTenTheLoai;
        $theloai->save();
        return $theloai;
    }
    public static function deleteTheLoai($id)
    {
        $theloai = theloai::where('id',$id);
        $theloai->delete();;
        return $theloai;
    }
}
