<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\cauhoi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use DB;
use App\Quotation;


class traloi extends Model
{
    //
    protected $table = "traloi";
    public function cauhoi()
    {
    	return $this->belongsTo('App\cauhoi','cauhoiid','id');
    }
    public function User()
    {
    	return $this->belongsTo('App\User','nguoitraloiid','id');
    }
    public static function getTraLoiByIdCauHoi($id)
    {
        return traloi::where('cauhoiid',$id)->get();
    }
    public static function postTraLoiByIdCauHoi($request,$id)
    {
        $traloi = new traloi;
        $traloi->cauhoiid = $id;
        $traloi->noidung = $request->txtNoiDung;
        $traloi->nguoitraloiid = Auth::user()->id;
        $traloi->save();
        return $traloi;
    }
}
