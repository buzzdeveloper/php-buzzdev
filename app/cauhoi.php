<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Quotation;
use App\traloi;
use App\danhmuc;

class cauhoi extends Model
{
    //
    protected $table = "cauhoi";
    public function User()
    {
    	return $this->belongsTo('App\User','nguoihoiid','id');
    }
    public function traloi()
    {
    	return $this->hasMany('App\traloi','cauhoiid','id');
    }
    public function danhmuc()
    {
    	return $this->belongsTo('App\danhmuc','danhmucid','id');
    }
    public static function getAllCauHoi()
    {
        return cauhoi::orderBy('created_at','desc')->paginate(10);
    }
    public static function getCauHoiById($id)
    {
        return cauhoi::find($id);
    }
    public static function getCauHoiByUser($id)
    {
        return cauhoi::where('nguoihoiid',$id)->get();
    }
    public static function getAllCauNoTraLoi()
    {
        return cauhoi::doesntHave('traloi')->orderBy('created_at','desc')->paginate(10);      
    }
    public static function getCauHoiNoTraLoiByDanhMuc($id)
    {
        return cauhoi::where('danhmucid',$id)->doesntHave('traloi')->orderBy('created_at','desc')->paginate(10);
    }
    public static function getCauHoiByDanhMuc($id)
    {
        return cauhoi::where('danhmucid',$id)->orderBy('created_at','desc')->paginate(10);
    }
    public static function get5CauHoiMoiNhat()
    {
        return cauhoi::orderBy('created_at','desc')->take(5)->get();
    }
    public static function postAddCauHoi($request)
    {
        $cauhoi = new cauhoi;
        $cauhoi->ten = $request->txtTieuDe;
        $cauhoi->danhmucid = $request->txtDanhMuc;
        $cauhoi->noidung = $request->txtNoiDung;
        $cauhoi->nguoihoiid = Auth::user()->id;
        $cauhoi->save();
        return $cauhoi;
    }
    //API
    public static function countCauHoi()
    {
        return cauhoi::count();
    }
    public static function countCauHoiNoTraLoi()
    {
        return cauhoi::doesntHave('traloi')->count();
    }
    public static function countCauHoiByDanhMuc($id)
    {
        return cauhoi::where('id',$id)->count();
    }
    public static function countCauHoiNoTraLoiByDanhMuc($id)
    {
        return cauhoi::doesntHave('traloi')->where('id',$id)->count();
    }
    public static function apiGetAllCauHoi($page)
    {
        return cauhoi::orderBy('created_at','desc')->skip(($page-1)*5)->take(5)->get();
    }
    public static function apiGetAllCauNoTraLoi($page)
    {
        return cauhoi::doesntHave('traloi')->orderBy('created_at','desc')->skip(($page-1)*5)->take(5)->get();   
    }
    public static function apiGetCauHoiNoTraLoiByDanhMuc($id,$page)
    {
        return cauhoi::where('danhmucid',$id)->doesntHave('traloi')->orderBy('created_at','desc')->skip(($page-1)*5)->take(5)->get();
    }
    public static function apiGetCauHoiByDanhMuc($id,$page)
    {
        return cauhoi::where('danhmucid',$id)->orderBy('created_at','desc')->skip(($page-1)*5)->take(5)->get();
    }
}
