<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraloiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traloi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cauhoiid')->unsigned();
            $table->integer('nguoitraloiid')->unsigned();
            $table->longText('noidung');
            $table->foreign('cauhoiid')->references('id')->on('cauhoi');
            $table->foreign('nguoitraloiid')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TraLoi');
    }
}
