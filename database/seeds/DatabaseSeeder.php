<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersincrementsTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('test123'),
            'level' => '4'
        ]);
        DB::table('users')->insert([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('test123'),
            'level' => '0'
        ]);
        DB::table('theloai')->insert([
            'ten' => 'Tin tức',
        ]);
        DB::table('theloai')->insert([
            'ten' => 'Thông báo',
        ]);
        DB::table('theloai')->insert([
            'ten' => 'Cẩm nang',
        ]);
    }
}
