@extends('layouts.NguoiDungLayout')
	@section('content')
<div class="container" >
    <div class="row no-margin">
        <div class="col-xs-12 col-md-3" id="category">
        	<div class="row">
        		<div class="col-xs-12 col-md-12">
		    		<h3 class="no-margin">Danh mục</h3>
		            <ul style="list-style-type: none;">
		            @if(isset($danhmuc))
		            	@foreach($danhmuc as $dm)
		            	<li><a href="{{asset('hoi-dap/danh-muc/'.$dm->id)}}">{{$dm->ten}}</a></li>
		            	@endforeach
		            @endif
		            </ul>
		        </div>
	        </div>
	        <div class="row">
			    <div class="col-xs-12 col-sm-12">
			        <h2>Câu hỏi mới</h2>
			        <ul style="list-style-type: none;">
			            @if(isset($cauhoimoi))
				            @foreach($cauhoimoi as $chm)
				            <li>
				                <p style="list-style: none;">{{$chm->created_at}}</p>
				                <a href="{{asset('hoi-dap/cau-hoi/'.$chm->id)}}">{{$chm->ten}}</a>
				            </li>
				            @endforeach
				        @endif
			        </ul>
			    </div>
			</div>
			@include('layouts.MenuRight.QuangCao')	
        </div>
        <div class="col-xs-12 col-md-9 padding-right">
        	<div class="row well">
				<div class="col-xs-12 col-md-12" id="answer">
					<h3>Hỏi đáp</h3>
				</div>
				@if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @elseif(session('thongbaoloi'))
                	<div class="alert alert-danger">
                        {{session('thongbaoloi')}}
                    </div>
                @endif

				<div class="col-xs-12 col-md-12" id="reply-answer">
					<ul style="list-style-type: none;">
						<li>
						<div class="row">							
							<div class="col-xs-12 col-md-2">
		        				<div class="thumbnail thumbnail-none">
		                        	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                        </div>
	                        </div>
                            <div class="col-xs-12 col-md-10">
                            	<div class="col-xs-12 a-title-top">
									<h5 class="pull-left no-margin">{{$cauhoi->User->name}} ({{$cauhoi->User->email}}) </h5>
									<p class="pull-right no-margin">Ngày đăng: {{$cauhoi->created_at}}</p>
								</div>
								<div class="col-xs-12">
									<h4 class="no-margin">{{$cauhoi->ten}}</h4>
	                                <i><p class="by-author text-overflow-ellipsis">
	                                    {!!$cauhoi->noidung!!}                  
	                                </p></i>
								</div>
							</div>
							</div>
						</li>
						<hr>
						<li>
						@foreach($traloi as $tl)
						<div class="row">
							<div class="col-xs-12 col-md-2">
		        				<div class="thumbnail thumbnail-none">
		                        	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                        </div>
	                        </div>
                            <div class="col-xs-12 col-md-10">
                            	<div class="col-xs-12 a-title-top">
									<h5 class="pull-left no-margin">{{$tl->User->name}} ({{$tl->User->email}}) </h5>
									<p class=" pull-right no-margin">Ngày đăng: {{$tl->created_at}}</p>
								</div>
								<div class="col-xs-12">
	                                <p class="by-author text-overflow-ellipsis">
	                                 	{{$tl->noidung}}                   
	                                </p>
								</div>
							</div>
						</div>
						@endforeach
						</li>
					</ul>
					<div class="line pull-right"></div>
						<!-- Comments Form -->
	            	<div>
		                <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
		                <form action="{{asset('hoi-dap/cau-hoi/'.$cauhoi->id)}}" method="POST" role="form">
		                	<input type="hidden" name="_token" value="{{csrf_token()}}"/>
		                    <div class="form-group">
		                        <textarea class="form-control" name="txtNoiDung" rows="3" placeholder="Viết bình luận..."></textarea>
		                    </div>
		                    <button type="submit" class="btn btn-primary">Gửi</button>
		                </form>
			        </div>
		            <!-- Posted Comments -->
		            <hr>
				</div>
				
				<div class="col-xs-12 col-md-12" id="related-questions">
					<h3>Chủ đề tương tự</h3>
					<ul style="list-style-type: none;">
						<li>
							<div class="row">
								<div class="row">
		                			<div class="col-xs-4 col-md-4">
		                				<div class="thumbnail thumbnail-none">
		                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                                </div>
		                            </div>
		                            <div class="col-xs-8 col-md-8">
		                            	<div class="col-xs-12 a-title-top">
											<h5 class="pull-left no-margin">Cao Tiến Lộc (...@gmail.com) </h5>
											<p class=" pull-right no-margin">Ngày đăng: 20-10-2017</p>
										</div>
										<div class="col-xs-12">
			                                <h4 class="no-margin">Có cần bổ sung vitamin PP?</h4>
			                                <p class="by-author text-overflow-ellipsis">
			                                    Vào dịp thời tiết lạnh, tôi rất hay bị loét miệng. Hôm vừa rồi một người bạn mách uống vitamin C và vitamin PP thì sẽ khỏi ngay. Vitamin C thì tôi đã biết, nhưng PP là loại vitamin gì, tôi có nên bổ sung không? Mong quý báo giải thích kỹ hơn. Tôi xin cảm ơn!                       
			                                </p>
			                                <a href="#" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
		                                <div>
		                            </div>
		                        </div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="row">
		                			<div class="col-xs-4 col-md-4">
		                				<div class="thumbnail thumbnail-none">
		                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                                </div>
		                            </div>
		                            <div class="col-xs-8 col-md-8">
		                            	<div class="col-xs-12 a-title-top">
											<h5 class="pull-left no-margin">Cao Tiến Lộc (...@gmail.com) </h5>
											<p class=" pull-right no-margin">Ngày đăng: 20-10-2017</p>
										</div>
										<div class="col-xs-12">
			                                <h4 class="no-margin">Có cần bổ sung vitamin PP?</h4>
			                                <p class="by-author text-overflow-ellipsis">
			                                    Vào dịp thời tiết lạnh, tôi rất hay bị loét miệng. Hôm vừa rồi một người bạn mách uống vitamin C và vitamin PP thì sẽ khỏi ngay. Vitamin C thì tôi đã biết, nhưng PP là loại vitamin gì, tôi có nên bổ sung không? Mong quý báo giải thích kỹ hơn. Tôi xin cảm ơn!                       
			                                </p>
			                                <a href="#" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
		                                <div>
		                            </div>
		                        </div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="row">
		                			<div class="col-xs-4 col-md-4">
		                				<div class="thumbnail thumbnail-none">
		                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                                </div>
		                            </div>
		                            <div class="col-xs-8 col-md-8">
		                            	<div class="col-xs-12 a-title-top">
											<h5 class="pull-left no-margin">Cao Tiến Lộc (...@gmail.com) </h5>
											<p class=" pull-right no-margin">Ngày đăng: 20-10-2017</p>
										</div>
										<div class="col-xs-12">
			                                <h4 class="no-margin">Có cần bổ sung vitamin PP?</h4>
			                                <p class="by-author text-overflow-ellipsis">
			                                    Vào dịp thời tiết lạnh, tôi rất hay bị loét miệng. Hôm vừa rồi một người bạn mách uống vitamin C và vitamin PP thì sẽ khỏi ngay. Vitamin C thì tôi đã biết, nhưng PP là loại vitamin gì, tôi có nên bổ sung không? Mong quý báo giải thích kỹ hơn. Tôi xin cảm ơn!                       
			                                </p>
			                                <a href="#" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
		                                <div>
		                            </div>
		                        </div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="row">
		                			<div class="col-xs-4 col-md-4">
		                				<div class="thumbnail thumbnail-none">
		                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                                </div>
		                            </div>
		                            <div class="col-xs-8 col-md-8">
		                            	<div class="col-xs-12 a-title-top">
											<h5 class="pull-left no-margin">Cao Tiến Lộc (...@gmail.com) </h5>
											<p class=" pull-right no-margin">Ngày đăng: 20-10-2017</p>
										</div>
										<div class="col-xs-12">
			                                <h4 class="no-margin">Có cần bổ sung vitamin PP?</h4>
			                                <p class="by-author text-overflow-ellipsis">
			                                    Vào dịp thời tiết lạnh, tôi rất hay bị loét miệng. Hôm vừa rồi một người bạn mách uống vitamin C và vitamin PP thì sẽ khỏi ngay. Vitamin C thì tôi đã biết, nhưng PP là loại vitamin gì, tôi có nên bổ sung không? Mong quý báo giải thích kỹ hơn. Tôi xin cảm ơn!                       
			                                </p>
			                                <a href="#" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
		                                <div>
		                            </div>
		                        </div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="row">
		                			<div class="col-xs-4 col-md-4">
		                				<div class="thumbnail thumbnail-none">
		                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
		                                </div>
		                            </div>
		                            <div class="col-xs-8 col-md-8">
		                            	<div class="col-xs-12 a-title-top">
											<h5 class="pull-left no-margin">Cao Tiến Lộc (...@gmail.com) </h5>
											<p class=" pull-right no-margin">Ngày đăng: 20-10-2017</p>
										</div>
										<div class="col-xs-12">
			                                <h4 class="no-margin">Có cần bổ sung vitamin PP?</h4>
			                                <p class="by-author text-overflow-ellipsis">
			                                    Vào dịp thời tiết lạnh, tôi rất hay bị loét miệng. Hôm vừa rồi một người bạn mách uống vitamin C và vitamin PP thì sẽ khỏi ngay. Vitamin C thì tôi đã biết, nhưng PP là loại vitamin gì, tôi có nên bổ sung không? Mong quý báo giải thích kỹ hơn. Tôi xin cảm ơn!                       
			                                </p>
			                                <a href="#" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
		                                <div>
		                            </div>
		                        </div>
							</div>
						</li>
					</ul>
				</div>

			</div>
            
        </div>
    </div>
</div>
<!-- ============= -->
@endsection
