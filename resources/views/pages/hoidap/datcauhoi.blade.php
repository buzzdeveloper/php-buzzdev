@extends('layouts.NguoiDungLayout')
@section('content')
<div class="container" style="margin-top: 20px;">
	<div class="row">
		<div class="col-xs-12 col-md-3" id="category">
            <ul style="list-style-type: none;">
            	<h3 class="no-margin">Danh mục</h3>
            	@foreach($danhmuc as $dm)
            	<li><a href="{{asset('hoi-dap/danh-muc/'.$dm->id)}}">{{$dm->ten}}</a></li>
            	@endforeach
            </ul>
	    </div>
	    <div class="col-xs-12 col-md-9">
	    	<div class="row">
	    		<div class="col-xs-12 col-md-8">
	    			@if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
			    	<form action="hoi-dap/dat-cau-hoi" method="POST" enctype="multipart/form-data">
		                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
		                <div class="form-group">
		                    <label>Danh mục</label>
		                    <select class="form-control" name="txtDanhMuc">
		                        @foreach($danhmuc as $danhmuc)
		                            <option value="{{$danhmuc->id}}">{{$danhmuc->ten}}</option>
		                        @endforeach
		                    </select>
		                </div>
		                <div class="form-group">
		                    <label>Tiêu đề</label>
		                    <input class="form-control" name="txtTieuDe" placeholder="Nhập tiêu đề" />
		                </div>
		                <div class="form-group">
		                    <label>Nội dung</label>
		                    <textarea class="ckeditor" id="demo" name="txtNoiDung"></textarea> 
		                </div>
		                <button type="submit" class="btn btn-primary">Đồng ý</button>
		                <button type="reset" class="btn btn-default">Nhập lại</button>
		            </form>
		        </div>
		        <div class="col-xs-12 col-md-4">
		        <div class="row">
				    <div class="col-xs-12 col-sm-12">
				        <h2>Câu hỏi mới</h2>
				        <ul>
				       	@if(isset($cauhoimoi))
				            @foreach($cauhoimoi as $chm)
				            <li>
				                <p style="list-style: none;">{{$chm->created_at}}</p>
				                <a href="{{asset('hoi-dap/cau-hoi/'.$chm->id)}}">{{$chm->ten}}</a>
				            </li>
				            @endforeach
				        @endif
				        </ul>
				    </div>
				</div>
		        @include('layouts.MenuRight.QuangCao')
		        </div>
	        </div>
	    </div>
    </div>
</div>
@endsection
