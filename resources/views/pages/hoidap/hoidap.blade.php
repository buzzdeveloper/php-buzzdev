@extends('layouts.NguoiDungLayout')
@section('content')
<div class="container" style="margin-top: 20px;" >
	<div class="row no-margin">
	    <div class="col-xs-12 col-md-3" id="category">
			<h3 class="no-margin">Danh mục</h3>
	        <ul style="list-style-type: none;">
	        @if(isset($danhmuc))
		        @foreach($danhmuc as $dm)
		        	<li><a href="{{asset('hoi-dap/danh-muc/'.$dm->id)}}">{{$dm->ten}}</a></li>
		        @endforeach
		    @endif
	        </ul>
	    </div>
	    <div class="col-xs-12 col-md-9 padding-right">
	        <div class="row">
	        	<div class="col-xs-12 col-md-8">
	        		<a href="{{asset('hoi-dap/dat-cau-hoi')}}" class="btn btn-primary">Đặt câu hỏi</a>
	        		<div class="row">
						<div class="col-xs-12" id="answer">
							<h3>Hỏi đáp</h3>
							<ul class="tabs">
								<li class="current" data-tab="tab-1">Danh sách câu hỏi</li>
								<li data-tab="tab-2">Câu hỏi chưa trả lời</li>
							</ul>
						</div>
						<!-- end #answer -->
						<div class="col-xs-12 no-padding category-answers current" id="tab-1">
							<ul style="list-style-type: none;">
							@if(isset($cauhoi))
								@foreach($cauhoi as $ch)
			            		<li>
			                        <!-- <a href="#"> -->
			                			<div class="col-xs-12 col-sm-3">
			                				<div class="thumbnail thumbnail-none">
			                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
			                                </div>
			                            </div>
			                            <div class="col-xs-12 col-sm-9">
			                            	<div class="col-xs-12 a-title-top">
												<h5 class="pull-left no-margin">{{$ch->User->name}} ({{$ch->User->email}})</h5>
												<p class=" pull-right no-margin">Ngày đăng: {{$ch->created_at}}</p>
											</div>
											<div class="col-xs-12">
				                                <h4 class="no-margin">{{$ch->ten}}</h4>
				                                <p class="by-author text-overflow-ellipsis">
				                                    {!!$ch->noidung!!}                     
				                                </p>
				                                <a href="{{asset('hoi-dap/cau-hoi/'.$ch->id)}}" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
			                                <div>
			                            </div>
			                        <!-- </a> -->
			            		</li>
			            		@endforeach
							</ul>
							<div style="margin-left: 15px;" id="paging-website">
			            		{!!$cauhoi->links()!!}
			            	</div>
			            	@endif
						</div>
						<!-- end .category-answers -->
						<div class="col-xs-12 no-padding category-answers" id="tab-2">
							<ul style="list-style-type: none;">
							@if(isset($cauhoinotraloi))
								@foreach($cauhoinotraloi as $chntl)
			            		<li>
			                        <!-- <a href="#"> -->
			                			<div class="col-xs-12 col-sm-3">
			                				<div class="thumbnail thumbnail-none">
			                                	<img src="{{asset('user_asset/i/camnang/camnang.jpg')}}" alt="" class="no-margin">
			                                </div>
			                            </div>
			                            <div class="col-xs-12 col-sm-9">
			                            	<div class="col-xs-12 a-title-top">
												<h5 class="pull-left no-margin">{{$chntl->User->name}} ({{$chntl->User->email}})</h5>
												<p class=" pull-right no-margin">Ngày đăng: {{$chntl->created_at}}</p>
											</div>
											<div class="col-xs-12">
				                                <h4 class="no-margin">{{$chntl->ten}}</h4>
				                                <p class="by-author text-overflow-ellipsis">
				                                    {!!$chntl->noidung!!}                   
				                                </p>
				                                <a href="{{asset('hoi-dap/cau-hoi/'.$chntl->id)}}" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
			                                <div>
			                            </div>
			                        <!-- </a> -->
			            		</li>
			            		@endforeach
			            	@endif
							</ul>
						</div>
						<!-- end .category-answers -->
					</div>
	        	</div>
	        	<div class="col-xs-12 col-md-4">
	        		<div class="row">
					    <div class="col-xs-12 col-sm-12">
					        <h2>Câu hỏi mới</h2>
					        <ul style="list-style-type: none;">
					        @if(isset($cauhoimoi))
					            @foreach($cauhoimoi as $chm)
					            <li>
					                <p style="list-style: none;">{{$chm->created_at}}</p>
					                <a href="{{asset('hoi-dap/cau-hoi/'.$chm->id)}}">{{$chm->ten}}</a>
					            </li>
					            @endforeach
					        @endif
					        </ul>
					    </div>
					</div>
			        @include('layouts.MenuRight.QuangCao')
	        	</div>
	        </div>
	    </div>
	</div>
</div>
<!-- ============= -->
@endsection

@section('script')
<script>
	$(document).ready(function(){
	
		$('ul.tabs li').click(function(){
			var tab_id = $(this).attr('data-tab');

			$('ul.tabs li').removeClass('current');
			$('.category-answers').removeClass('current');

			$(this).addClass('current');
			$("#"+tab_id).addClass('current');
		})

	});
</script>
@endsection