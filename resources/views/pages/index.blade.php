@extends('layouts.NguoiDungLayout')
@section('content')
<div class="container">
    <div class="row ">
        <div class="carousel fade-carousel slide col-lg-12" data-ride="carousel" data-interval="4000" id="header-slide">
            <!-- Overlay -->
            <div class="overlay"></div>
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#header-slide" data-slide-to="0" class="active"></li>
                <li data-target="#header-slide" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item slides active">
                    <img src="{{asset('user_asset/i/slider/3.png')}}" width="99%">
                </div>
                <div class="item slides">
                    <img src="{{asset('user_asset/i/slider/4.png')}}" width="99%">
                </div>
            </div>
        </div>
    </div>
    @include('layouts.MenuItemsPageNguoiDung')
    <div class="row">
        <div class="col-lg-8">
            <div class="row">
                <div class="col-lg-8">
                @if(isset($tintuc))

                    <?php $tinchinh = $tintuc->shift(); ?>
                    @if(isset($tinchinh))
                    <div class="caption">
                        <a href="tin-tuc/{{$tinchinh->id}}/{{$tinchinh->tieudekhongdau}}.html">
                            <img src="upload/tintuc/{{$tinchinh['hinhanh']}}" alt="" class="thumb" style="width: 100%; height: 350px;">
                        </a>
                        <h3>{{$tinchinh['tieude']}}</h3>
                        <p class="by-author">
                        {{$tinchinh['tomtat']}}
                        </p>
                    </div>
                    @endif
                @endif
                </div>
                <div class="col-lg-4" id="latest-news">
                    <h2>Tin mới nhất</h2>
                    <ul>
                    @if(isset($tintuc))
                        @foreach($tintuc as $tt)
                            <li>
                                <a href="tin-tuc/{{$tt->id}}/{{$tt->tieudekhongdau}}.html">{{$tt->tieude}}</a>
                                <hr>
                            </li>
                        @endforeach
                    @endif
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <img src="{{asset('user_asset/i/bannertiemvacxin_dseho.gif')}}" width="99%" alt="#">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <span>Sản phẩm mới</span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                    @if(isset($sanpham))
                        @foreach($sanpham as $sp)
                            <div class="col-xs-6 col-sm-3">
                                <img src="{{asset('upload/sanpham/'.$sp->hinhanh)}}" width="99%" alt="#"/>
                                <p style="text-align: center;">{{$sp->ten}}</p>
                            </div>
                        @endforeach
                    @endif
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6"><h2>Cẩm nang</h2></div>
                        <div class="col-lg-6"><p>Xem thêm</p></div>
                    </div>
                </div>
                <div class="col-lg-6">
                @if(isset($camnang))
                    <?php $tincamnangchinh = $camnang->shift(); ?>
                    @if(isset($tincamnangchinh))
                    <div class="caption">
                        <a href="{{asset('tin-tuc/'.$tincamnangchinh->id.'/'.$tincamnangchinh->tieudekhongdau)}}.html">
                            <img src="{{asset('upload/tintuc/'.$tincamnangchinh['hinhanh'])}}" alt="" class="thumb" style="width: 100%; height: 350px;">
                        </a>
                        <h3>{{$tincamnangchinh['tieude']}}</h3>
                        <p class="by-author">
                        {{$tincamnangchinh['tomtat']}}
                        </p>
                    </div>
                    @endif
                @endif
                </div>
                <div class="col-lg-6" id="latest-news">
                    <ul>
                @if(isset($camnang))
                    @foreach($camnang as $cn)
                        <li>
                            <div class="row">
                                <div class="col-xs-4 col-sm-4">
                                    <img src="upload/tintuc/{{$cn->hinhanh}}" alt="" class="thumb" width="99%">
                                </div>
                                <div class="col-xs-8 col-sm-8">
                                    <a href="tin-tuc/{{$cn->id}}/{{$cn->tieudekhongdau}}.html">{{$cn->tieude}}</a>
                                    <p>{{$cn->tomtat}}</p>
                                </div>
                                <hr>
                            </div>
                        </li>
                    @endforeach
                @endif
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="row" >
                <div class="col-lg-12">
                    <div><h2>Thông báo</h2></div>
                    <div>
                        <ul style="list-style-type: none;">
                        @if(isset($thongbao))
                            @foreach($thongbao as $tb)
                            <li>
                                <p>Ngày đăng: {{$tb->created_at}}</p>
                                <a href="{{asset('tin-tuc/'.$tb->id.'/'.$tb->tieudekhongdau)}}.html">{{$tb->tieude}}</a>
                            </li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
            </div> 
           @include('layouts.MenuRight.QuangCao') 
        </div>
    </div>
</div>
@endsection