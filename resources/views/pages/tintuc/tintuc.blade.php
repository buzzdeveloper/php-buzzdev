@extends('layouts.NguoiDungLayout')
@section('content')
<div class="container">
    <div class="row no-margin">
        <div class="col-xs-12 col-md-2">
            @include('layouts.MenuItemsPageNguoiDung')
        </div>
        <div class="col-xs-12 col-md-7 border-solid all-tin-tuc">
            <div class="col-xs-12">
                <h3>Tin tức</h3>
            </div>
            <ul>
            @if(isset($tintuc))
                @foreach($tintuc as $tt)
                <li>
                    <div class="col-xs-12 col-sm-3">
                        <div class="thumbnail thumbnail-none">
                            <a href="{{asset('chi-tiet/'.$tt->id.'/'.$tt->tieudekhongdau)}}.html">
                                <img src="{{asset('upload/tintuc/'.$tt->hinhanh)}}" alt="" class="no-margin"/>
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <h4 class="no-margin"><a href="{{asset('chi-tiet/'.$tt->id.'/'.$tt->tieudekhongdau)}}.html">{{$tt->tieude}}</a></h4>
                        <p class="date-posted">Ngày đăng: {{$tt->created_at}}</p>
                        <p class="by-author">
                            Nam Nguyen                     
                        </p>
                        <a href="{{asset('chi-tiet/'.$tt->id.'/'.$tt->tieudekhongdau)}}.html" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
                    </div>
                </li>
                @endforeach
            </ul>
            <div>
                {{$tintuc->links()}}
            </div>
            @endif
        </div>
        <div class="col-xs-12 col-md-3" id="category">
            <h3 class="no-margin">Danh mục</h3>
            <ul style="list-style-type: none;">
            @if(isset($theloaitin))
                @foreach($theloaitin as $tlt)
                    <li><a href="{{asset('the-loai/'.$tlt->id)}}">{{$tlt->ten}}</a></li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
</div>
@endsection