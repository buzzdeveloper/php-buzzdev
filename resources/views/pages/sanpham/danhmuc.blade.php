@extends('layouts.NguoiDungLayout')
@section('content')
<div class="container">
    <div class="row no-margin">
        <div class="col-xs-12 col-md-2">
            @include('layouts.MenuItemsPageNguoiDung')
        </div>
        <div class="col-xs-12 col-md-7 border-solid all-tin-tuc">
            <div class="col-xs-12">
                <h3>Sản phẩm</h3>
            </div>
            <ul>
                @foreach($sanpham as $sp)
                <li>
                    <a href="#">
                        <div class="col-xs-12 col-sm-3">
                            <div class="thumbnail thumbnail-none">
                                <img src="{{asset('upload/sanpham/'.$sp->hinhanh)}}" alt="" class="no-margin">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <h4 class="no-margin">{{$sp->tieude}}</h4>
                            <p class="date-posted">Ngày đăng: {{$sp->created_at}}</p>
                            <p class="by-author">
                                Nam Nguyen                     
                            </p>
                            <a href="{{asset('san-pham/'.$sp->id.'/'.$sp->ten)}}.html" class="btn-xemthem"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> Xem thêm</a>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
            <div>
                {{$sanpham->links()}}
            </div>
        </div>
        <div class="col-xs-12 col-md-3" id="category">
            <h3 class="no-margin">Danh mục</h3>
            <ul style="list-style-type: none;">
            @if(isset($danhmucsanpham))
                @foreach($danhmucsanpham as $tlsp)
                    <li><a href="{{asset('san-pham/'.$tlsp->id)}}">{{$tlsp->ten}}</a></li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
</div>
@endsection