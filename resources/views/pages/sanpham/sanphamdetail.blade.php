@extends('layouts.NguoiDungLayout')
@section('content')
<!-- Page Content -->
<div class="container" style="margin-top: 20px;">
    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-9">
            <div class="row">
                <div class="col-lg-5">
                    <img width="99%" src="{{asset('upload/sanpham/'.$sanpham->hinhanh)}}" 
                            alt="{{$sanpham->ten}}" class="img-responsive img-thumbnail">
                </div>
                <div class="col-lg-7">
                    <h3>{{$sanpham->ten}}</h3>
                    <p>giá: {{$sanpham->gia}}</p>
                    <p>{{$sanpham->tomtat}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p>{!!$sanpham->noidung!!}</p>
                </div>
            </div>
            <!-- Blog Comments -->

            <!-- Comments Form -->
            <div class="well">
                <h4>Viết bình luận ...<span class="glyphicon glyphicon-pencil"></span></h4>
                <form role="form">
                    <div class="form-group">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Gửi</button>
                </form>
            </div>

            <hr>

            <!-- Posted Comments -->

            <!-- Comment -->
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">Start Bootstrap
                        <small>August 25, 2014 at 9:30 PM</small>
                    </h4>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
            </div>

            <!-- Comment -->
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object" src="http://placehold.it/64x64" alt="">
                </a>
                <div class="media-body">
                    <h4 class="media-heading">Start Bootstrap
                        <small>August 25, 2014 at 9:30 PM</small>
                    </h4>
                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                </div>
            </div>
        </div>
        <div class="col-lg-3" id="category">
            <h3 class="no-margin">Danh mục</h3>
            <ul style="list-style-type: none;">
            @if(isset($danhmucsanpham))
                @foreach($danhmucsanpham as $tlsp)
                    <li><a href="{{asset('san-pham/'.$tlsp->id)}}">{{$tlsp->ten}}</a></li>
                @endforeach
            @endif
            </ul>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- end Page Content -->
@endsection