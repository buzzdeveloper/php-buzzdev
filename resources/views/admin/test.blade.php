
<!DOCTYPE html>
<html>
<head>
	<title>test</title>
</head>
<body>
	@if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach($errors->all() as $err)
                {{$err}}<br>
            @endforeach
        </div>
	@endif
	@if(session('thongbao'))
		{{session('thongbao')}}
	@endif
	<div>{{$cauhoi->noidung}}</div>
	<form action="../test/{{$cauhoi->id}}" method="POST">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<textarea name="txtNoiDung"></textarea>
		<input type="submit" name="btn-submit">
	</form>
</body>
</html>