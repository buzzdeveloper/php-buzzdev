<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{asset('admin_asset/css/styleloginadmin.css')}}" rel="stylesheet">
        <link href="{{asset('admin_asset/css/bootstrap.min.css')}}" rel="stylesheet">
        <base href="{{asset('')}}">
    </head>
    <body>
        <div class="col-md-4 col-md-offset-4 main">
            <h3>ADMIN LOGIN</h3>
            <div class="line">
            
            </div>
            @if(count($errors)>0)
              <div>
                @foreach($errors->all() as $err)
                    {{$err}}
                @endforeach
              </div>
            @endif
            @if(session('thongbao') || session('loi'))
              <div>{{session('thongbao')}}</div>
              <div>{{session('loi')}}</div>
            @endif
            <form class="form-horizontal" action="admin/login" method="POST">
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="form-group">
                <label for="inputEmail3" class="col-md-offset-1 col-sm-2 control-label">Email</label>
                <div class="col-sm-8">
                  <input type="email" class="form-control" name="txtEmail" placeholder="Nhập email hoặc số điện thoại.">
                </div>
              </div>
              <div class="form-group">
                <label for="inputPassword3" class="col-md-offset-1 col-sm-2 control-label">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="txtPassWord" placeholder="Nhập mật khẩu đăng nhập.">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
                  <button type="submit" class="btn-login">Sign in</button>
                </div>
              </div>
            </form>
        </div>        
    </body>
</html>