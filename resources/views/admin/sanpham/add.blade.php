@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản phẩm
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(session('messenger'))
                        <div class="alert alert-danger">
                            {{session('messenger')}}
                        </div>
                    @endif
                        <form action="admin/sanpham/add" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label>Danh mục</label>
                                <select class="form-control" name="txtDanhMuc">
                                    @foreach($danhmucsanpham as $dmsp)
                                        <option value="{{$dmsp->id}}">{{$dmsp->ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input type="Text" class="form-control" name="txtTenSanPham" placeholder="Nhập tên sản phẩm" />
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <input type="Text" class="form-control" name="txtTomTat" placeholder="Nhập tóm tắt" />
                            </div>
                            <div class="form-group">
                                <label>Thông tin thuốc</label>
                                <textarea class="ckeditor" id="demo" name="txtNoiDung"></textarea> 
                            </div>
                            <div class="form-group">
                                <label>Giá sản phẩm</label>
                                <input type="number" class="form-control" name="txtGiaSanPham" placeholder="Nhập giá sản phẩm" />
                            </div>
                            <div class="form-group">
                                <label>Hình Ảnh</label>
                                <input type="file" class="form-control" name="txtHinhAnh"/>
                            </div>
                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
