@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản phẩm
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(session('messenger'))
                        <div class="alert alert-danger">
                            {{session('messenger')}}
                        </div>
                    @endif

                        <form action="admin/sanpham/edit/{{$sanpham->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label>Danh mục</label>
                                <select class="form-control" name="txtTheLoai">
                                    @foreach($danhmucsanpham as $dmsp)
                                        <option 
                                        @if($sanpham->danhmucid == $dmsp->id)
                                            {{"selected"}}
                                        @endif
                                        value="{{$dmsp->id}}">{{$dmsp->ten}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input class="form-control" name="txtTenSanPham" value="{{$sanpham->ten}}" placeholder="Nhập tên sản phẩm" />
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <input class="form-control" name="txtTomTat" value="{{$sanpham->tomtat}}" placeholder="Nhập tóm tắt" />
                            </div>
                            <div class="form-group">
                                <label>Thông tin thuốc</label>
                                <textarea class="ckeditor" id="demo" name="txtNoiDung">{{$sanpham->noidung}}
                                </textarea> 
                            </div>
                            <div class="form-group">
                                <label>Giá sản phẩm</label>
                                <input type="number" class="form-control" name="txtGiaSanPham" value="{{$sanpham->gia}}" placeholder="Nhập giá sản phẩm" />
                            </div>
                            <div class="form-group">
                                <label>Hình Ảnh</label>
                                <div style="width: 200px">
                                <img name="img" src="{{asset('upload/sanpham/'.$sanpham->hinhanh)}}" width="200px" class="img-thumbnail"><br><p style="text-align: center;">{{$sanpham->hinhanh}}</p></div>
                                <p id="demo"></p>
                                <input id="myFile" type="file" class="form-control" name="txtHinhAnh" />
                            </div>
                            <button type="submit" class="btn btn-primary" onclick="myFunction()">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
<script>
        function myFunction() {
             var x = document.getElementById("myFile").value;
            document.getElementById("demo").innerHTML = x;
        }
</script>