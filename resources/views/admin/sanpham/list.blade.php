@extends('admin.layout.index')
@section('content')
<!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Sản phẩm
                        <small>Danh sách</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(session('thongbao'))
                    <div class="alert alert-success">
                        {{session('thongbao')}}
                    </div>
                @endif
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr align="center">
                            <th style="width: 5%">ID</th>
                            <th style="width: 10%">Tên sản phẩm</th>
                            <th style="width: 20%">Tóm tắt</th>
                            <th style="width: 35%">Thông tin thuốc</th>
                            <th style="width: 15%">Hình ảnh</th>
                            <th style="width: 5%">Danh mục</th>
                            <th style="width: 5%">Delete</th>
                            <th style="width: 5%">Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(isset($sanpham))
                        @foreach($sanpham as $sp)
                            <tr class="odd gradeX" align="center">
                                <td>{{$sp->id}}</td>
                                <td>{{$sp->ten}}</td>
                                <td>{{$sp->tomtat}}</td>
                                <td>{{$sp->noidung}}</td>
                                <td><img src="{{asset('upload/sanpham/'.$sp->hinhanh)}}" width="150px" class="img-thumbnail" /></td>
                                <td>{{$sp->danhmuc->ten}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="{{asset('admin/sanpham/delete/'.$sp->id)}}">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{{asset('admin/sanpham/edit/'.$sp->id)}}">Edit</a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@endsection