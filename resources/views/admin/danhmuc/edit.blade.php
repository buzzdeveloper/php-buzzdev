 		@extends('admin.layout.index')

 		@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh mục câu hỏi
                            <small>{{$danhmuc->Ten}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                        <form action="{{asset('admin/danhmuc/edit/'.$danhmuc->id)}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label>Chức năng</label>
                                <select class="form-control" name="txtMenu">
                                @if($danhmuc->menu == 1)
                                    <option value="1">Hỏi đáp</option>
                                @else
                                    <option value="0">Sản phẩm</option>
                                @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tên danh mục</label>
                                <input class="form-control" name="txtTenDanhMuc" placeholder="Nhập tên thể loại" value="{{$danhmuc->ten}}" />
                            </div>
                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        @endsection