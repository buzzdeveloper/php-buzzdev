@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tin Tức
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                    @if(session('messenger'))
                        <div class="alert alert-danger">
                            {{session('messenger')}}
                        </div>
                    @endif
                        <form action="admin/tintuc/add" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select class="form-control" name="txtTheLoai" id="idTheLoai">
                                    @foreach($theloai as $tl)
                                        <option value="{{$tl->id}}">{{$tl->ten}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="txtTieuDe" placeholder="Nhập tiêu đề" />
                            </div>
                            <div class="form-group">
                                <label>Tóm tắt</label>
                                <input class="form-control" name="txtTomTat" placeholder="Nhập tóm tắt" />
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                <textarea class="ckeditor" id="demo" name="txtNoiDung"></textarea> 
                            </div>
                            <div class="form-group">
                                <label>Hình Ảnh</label>
                                <input type="file" class="form-control" name="txtHinhAnh"/>
                            </div>
                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection
@section('script')
    <script>
        $(document).ready(function(){
            $("#idMenu").change(function(){
                var idMenu = $(this).val();
                $.get("admin/ajax/theloai/"+idMenu,function(data){
                    // alert(data);
                    $("#idTheLoai").html(data);
                });
            });
        });
    </script>
@endsection