 		@extends('admin.layout.index')

 		@section('content')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>{{$users->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                        <form action="admin/users/edit/{{$users->id}}" method="POST">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                           <div class="form-group">
                                <label>Tên tài khoản</label>
                                <input type="Text" class="form-control" name="txtTaiKhoan" value="{{$users->name}}" placeholder="Nhập tài khoản" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="Email" class="form-control" name="txtEmail" value="{{$users->email}}" placeholder="Nhập email" />
                            </div>
                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input type="PassWord" class="form-control" name="txtMatKhau" value="{{$users->password}}" placeholder="Nhập mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="PassWord" class="form-control" name="txtNhapLaiMatKhau" value="{{$users->password}}" placeholder="Nhập lại mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="Text" class="form-control" name="txtLevel" value="{{$users->level}}" placeholder="Nhập level" />
                            </div>
                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
        @endsection