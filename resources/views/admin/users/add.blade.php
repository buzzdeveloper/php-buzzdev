@extends('admin.layout.index')

@section('content')
<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>Thêm</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                        <div class="alert alert-success">
                            {{session('thongbao')}}
                        </div>
                    @endif
                        <form action="admin/users/add" method="POST">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <div class="form-group">
                                <label>Tên tài khoản</label>
                                <input type="Text" class="form-control" name="txtTaiKhoan" placeholder="Nhập tài khoản" />
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="Email" class="form-control" name="txtEmail" placeholder="Nhập email" />
                            </div>
                            <div class="form-group">
                                <label>Mật khẩu</label>
                                <input type="PassWord" class="form-control" name="txtMatKhau" placeholder="Nhập mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu</label>
                                <input type="PassWord" class="form-control" name="txtNhapLaiMatKhau" placeholder="Nhập lại mật khẩu" />
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <select class="form-control" name="txtLevel">
                                    <option value="0">Người dùng</option>
                                    <option value="1">Chuyên gia</option>
                                    <option value="4">Admin</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-primary">Đồng ý</button>
                            <button type="reset" class="btn btn-default">Nhập lại</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection