	<footer>
	    <div class="row no-margin" id="footer">
	        <div class="container" id="about">
	            <div class="col-sm-3">
	                <ul>
	                    <li>
	                        <h4>VỀ CHÚNG TÔI</h4>
	                    </li>
	                    <li>
	                        <a href="#">Giới thiệu</a>
	                    </li>
	                    <li>
	                        <a href="#">Đối tác</a>
	                    </li>
	                    <li>
	                        <a href="#">Sứ mệnh</a>
	                    </li>
	                    <li>
	                        <a href="#">Newsletter</a>
	                    </li>
	                </ul>
	            </div>
	            <div class="col-sm-3">
	                <ul>
	                    <li>
	                        <h4>TÀI KHOẢN</h4>
	                    </li>
	                    <li>
	                        <a href="#">Đăng ký</a>
	                    </li>
	                    <li>
	                        <a href="#">Đăng nhập</a>
	                    </li>
	                    <li>
	                        <a href="#">Quản lý tài khoản</a>
	                    </li>
	                    <li>
	                        <a href="#">Điều khoản</a>
	                    </li>
	                </ul>
	            </div>
	            <div class="col-sm-3">
	                <ul>
	                    <li>
	                        <h4>HỖ TRỢ</h4>
	                    </li>
	                    <li>
	                        <a href="#">FAQ</a>
	                    </li>
	                    <li>
	                        <a href="#">Hướng dẫn</a>
	                    </li>
	                </ul>
	            </div>
	            <div class="col-sm-3">
	                <ul>
	                    <li>
	                        <h4>ĐĂNG KÝ NHẬN EMAIL</h4>
	                    </li>
	                    <li>
	                        <input type="text" class="form-control" name="email" value="" placeholder="Email Của Bạn">
	                    </li>
	                    <li>
	                        <a href="#">Theo dõi bạn trên</a>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>
	    <div class="row no-margin" id="copyright">
	        <div class="container">
	            <b>&copy; 2017</b>
	        </div>
	    </div>
	</footer>