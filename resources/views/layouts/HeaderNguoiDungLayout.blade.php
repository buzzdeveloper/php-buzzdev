<header>
    <nav class="navbar navbar-custom navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{asset('')}}"><img src="{{asset('user_asset/i/logo.png')}}" alt="">
                </a>
            </div>
            <div id="navbar3" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{asset('san-pham')}}">SẢN PHẨM</a></li>
                    <li><a href="{{asset('cam-nang/3')}}">CẨM NANG</a></li>
                    <li><a href="{{asset('co-so-y-te')}}">CƠ SỞ Ý TẾ</a></li>
                    <li><a href="{{asset('the-loai/1')}}">TIN TỨC</a></li>
                    <li><a href="{{asset('hoi-dap/cau-hoi')}}">HỎI ĐÁP</a></li>
                    <li>
                        <form action="" class="search-form">
    		                <div class="form-group has-feedback">
    		            		<label for="search" class="sr-only">Search</label>
    		            		<input type="text" class="form-control" name="search" id="search" placeholder="search">
    		              		<span class="glyphicon glyphicon-search form-control-feedback"></span>
    		            	</div>
    		            </form>
                    </li>
                    <li>
                        @if(Auth::check())
                            <div class="dropdown form-group">
                                <a data-toggle="dropdown">{{Auth::user()->name}}
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="#">Cá nhân</a></li>
                                  <li><a href="{{asset('logout')}}">Đăng xuất</a></li>
                                </ul>
                            </div>
                        @else
                            <a href="#">       	
    							<img src="{{asset('user_asset/i/account_header.png')}}" alt="" data-toggle="modal" data-target="#login-modal">
                            </a>
                        @endif
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
</header>

<!-- Modal -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1>Đăng nhập</h1><br>
          <form action="{{asset('login')}}" method="GET">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="text" name="txtEmail" placeholder="Email">
            <input type="password" name="txtPassWord" placeholder="Mật khẩu">
            <input type="submit" name="login" class="login loginmodal-submit" value="Đăng nhập">
          </form>
            
          <div class="login-help">
            <a href="#" data-toggle="modal" data-target="#signup-modal">Đăng ký</a> - <a href="#">Quên mật khẩu</a>
          </div>
        </div>
    </div>
</div>