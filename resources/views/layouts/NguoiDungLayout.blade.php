<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Vắc xin cho trẻ</title>	
	<link href="{{asset('user_asset/css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('user_asset/css/style.css')}}" rel="stylesheet">
</head>
<body>
	@include('layouts.HeaderNguoiDungLayout')

    @yield('content')
    
	@include('layouts.FooterNguoiDungLayout')



    <script src='{{asset('user_asset/js/jquery.min.js')}}'></script>
    <script src='{{asset('user_asset/js/bootstrap.min.js')}}'></script>
    <script type="text/javascript" language="javascript" src="admin_asset/ckeditor/ckeditor.js" ></script>
    @yield('script')
</body>
</html>