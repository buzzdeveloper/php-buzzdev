<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//view page user
//trang-chu
Route::get('','HomeController@index');
//tin-tuc
Route::get('the-loai/{id}','TinTucController@getAllTinTuc');
Route::get('chi-tiet/{id}/{tieudekhongdau}.html','TinTucController@getDetail');
//Người dùng
Route::get('register','HomeController@getRegister');
Route::post('register','HomeController@postRegister');
Route::get('login','HomeController@postLogin');
Route::get('logout','HomeController@getLogout');
//Hoi Dap
Route::get('hoi-dap/cau-hoi','HoiDapController@getAllCauHoi');
Route::get('hoi-dap/danh-muc/{id}','HoiDapController@getCauHoiByDanhMuc');
Route::get('hoi-dap/dat-cau-hoi','HoiDapController@getAddCauHoi');
Route::post('hoi-dap/dat-cau-hoi','HoiDapController@postAddCauHoi');
Route::get('hoi-dap/cau-hoi/{id}','HoiDapController@getCauHoiById');
Route::post('hoi-dap/cau-hoi/{id}','HoiDapController@postTraLoiByIdCauHoi');
//sản phẩm
Route::get('san-pham','SanPhamController@getAllSanPham');
Route::get('san-pham/{id}','SanPhamController@getSanPhamByTheLoai');
Route::get('san-pham/{id}/{tensanpham}.html','SanPhamController@getSanPhamByTheLoai');

//view page login admin
Route::get('admin/login','AdminLoginController@getLogin');
Route::post('admin/login','AdminLoginController@postLogin');
//admin
Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function(){
	Route::group(['prefix'=>'theloai'],function(){
		//admin/theloai/..
		Route::get('list','TheLoaiController@getList');
		Route::get('delete/{id}','TheLoaiController@getDelete');

		Route::get('edit/{id}','TheLoaiController@getEdit');
		Route::post('edit/{id}','TheLoaiController@postEdit');

		Route::get('add','TheLoaiController@getAdd');
		Route::post('add','TheLoaiController@postAdd');
	});
	Route::group(['prefix'=>'users'],function(){
		//admin/users/..
		Route::get('list','UserController@getList');
		Route::get('delete/{id}','UserController@getDelete');

		Route::get('edit/{id}','UserController@getEdit');
		Route::post('edit/{id}','UserController@postEdit');

		Route::get('add','UserController@getAdd');
		Route::post('add','UserController@postAdd');
	});
	Route::group(['prefix'=>'tintuc'],function(){
		//admin/tintuc/..
		Route::get('list','TinTucController@getList');
		Route::get('delete/{id}','TinTucController@getDelete');

		Route::get('edit/{id}','TinTucController@getEdit');
		Route::post('edit/{id}','TinTucController@postEdit');

		Route::get('add','TinTucController@getAdd');
		Route::post('add','TinTucController@postAdd');
	});
	Route::group(['prefix'=>'danhmuc'],function(){
		//admin/danhmuc/..
		Route::get('list','DanhMucController@getList');
		Route::get('delete/{id}','DanhMucController@getDelete');

		Route::get('edit/{id}','DanhMucController@getEdit');
		Route::post('edit/{id}','DanhMucController@postEdit');

		Route::get('add','DanhMucController@getAdd');
		Route::post('add','DanhMucController@postAdd');
	});
	Route::group(['prefix'=>'sanpham'],function(){
		//admin/sanpham/..
		Route::get('list','SanPhamController@getList');
		Route::get('delete/{id}','SanPhamController@getDelete');

		Route::get('edit/{id}','SanPhamController@getEdit');
		Route::post('edit/{id}','SanPhamController@postEdit');

		Route::get('add','SanPhamController@getAdd');
		Route::post('add','SanPhamController@postAdd');
	});
	Route::group(['prefix'=>'ajax'],function(){
		//admin/ajax/..
		Route::get('theloai/{idMenu}','AjaxController@getTheLoai');
	});
});

//API
Route::resource('api/login','APILoginController');
Route::post('api/login','APILoginController@postLogin');
//hoi dap
Route::get('api/hoidap/cauhoi/{page}','APIHoiDapController@apiGetAllCauHoi');
Route::get('api/hoidap/cauhoichuatraloi/{page}','APIHoiDapController@apiGetAllCauNoTraLoi');

Route::get('api/hoidap/cauhoi/danhmuc/{id}/{page}','APIHoiDapController@apiGetCauHoiByDanhMuc');
Route::get('api/hoidap/cauhoichuatraloi/danhmuc/{id}/{page}','APIHoiDapController@apiGetCauHoiNoTraLoiByDanhMuc');
Route::post('api/hoidap/datcauhoi','APIHoiDapController@postAddCauHoi');
Route::get('api/hoidap/cauhoi/{id}','APIHoiDapController@getCauHoiById');
Route::get('api/hoidap/traloi/{id}','APIHoiDapController@getTraLoiByIdCauHoi');
Route::post('api/hoidap/traloi/{id}','APIHoiDapController@postTraLoiByIdCauHoi');
//tin tuc
Route::get('api/theloai/{id}/{page}','APITinTucController@apiGetAllTinTuc');
Route::get('api/detail/{id}','APITinTucController@getDetail');
//the loai
Route::get('api/theloai','APITinTucController@apiGetAllTheLoai');
